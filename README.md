# Projet e-Commerce

L'objectif de ce projet est de réaliser un site de vente en ligne.
Nous avons pour cela réalisé un site de vente de t-shirts personnalisables.

[Lien vers le dépot frontend](https://gitlab.com/e-commerce-g4/e-commerce-back)

## Conception

[Lien vers les fichiers de conception](https://gitlab.com/e-commerce-g4/e-commerce-front/-/tree/main/doc)

### Diagrammes de Use Case
  * **Naviagation**
<img src="doc/img/Use Case - 1 - Naviagation.png" alt="" width="800">

  * **Commande**
<img src="doc/img/Use Case - 2 - Commande.png" alt="" width="800">

  * **Gestion du compte**
<img src="doc/img/Use Case - 3 - Gestion compte.png" alt="" width="800">

### **Diagramme de classe**
<img src="doc/img/Diag de classes.png" alt="" width="800">

### **Maquettes avec Draw.io**

## Technique
* Utilisation Spring Boot et JPA
* Utilisation de Angular
* Application responsive avec Bootstrap

## User Stories

1. En tant qu'utilisateur·ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix

*Possibilité de faire un tri par couleur et par genre depuit la page d'affichage de la liste des produits au moyen de listes déroulantes*

2. En tant qu'utilisateur·ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande

*L'ajout peut se faire soit depuis la page de visualisation de la liste des produits soit depuis la page d'un produit en cliquant sur le bouton 'Ajouter au panier' après avoir personnalisé le produit et sélectionné une quantité*

3. En tant que visiteur·euse, je veux pouvoir créer mon compte pour accéder à des fonctionnalités supplémentaires du site

*En créant un compte, le·la visiteur·euse peut passer commande à partir de son panier, accéder à l'historique de ses commandes, gérer ses adresses, modifier ses infomations personnelles ainsi que changer son mot de passe*

4. En tant qu'administrateur·ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue

*L'administateur a la possiblité de créer un nouveau produit ou d'en édité un déjà existant*

## Base de données

Les données sont générées au moyen du fixtureLoader.

Génération de 2 utilisateurs test dont le mot de passe est '1234' :

* user@test.com (avec ROLE_USER)
* admin@test.com (avec ROLE_ADMIN)
