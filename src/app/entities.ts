
export interface Address {
  id?: number;
  label: string;
  street: string;
  zipCode: string;
  city: string;
  country: string;
  user: User;
  orders: Order;
}

export interface Design {
  id?: number;
  label: string;
  image: string;
  items: Item;
}

export interface Item {
  id?: number;
  order?: Order;
  product: Product;
  quantity: number;
  size: Size;
  design: Design;
  price?: number;
}

export interface Order {
  id?: number;
  user: User;
  address: Address;
  payment?: string;
  status?: string;
  date?: string;
  items?: Item[];
}

export interface Product {
  id?: number;
  label: string;
  reference: string;
  price: number;
  color: string;
  gender: string;
  image: string;
  items?: Item[];
}

export interface Size {
  id?: number;
  label: string;
  items?: Item[];
}

export interface User {
  id?: number;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  password?: string;
  role?: string;
  addresses?: Address[];
  orders?: Order[];
}

export interface Page<T> {
  content: T[];

  totalPages: number;
  totalElements: number;
  last: boolean;
  first: boolean;

  size: number;
  number: number;
  numberOfElements: number;
  empty: boolean;
}
