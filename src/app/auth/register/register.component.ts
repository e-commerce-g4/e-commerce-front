import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { hasOnlyLettersAndSpaceValidator, hasOnlyNumbersValidator } from 'src/app/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string = '';
  password: string = '';
  hasError = false;

  user = this.auth.state.user;

  form = this.fb.group({
    firstName: ['', [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    lastName: ['', [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), hasOnlyNumbersValidator()]],
    address: ["", [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    city: ["", [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    country: ["", [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    zipCode: ["", [Validators.required, Validators.minLength(5), Validators.maxLength(5), hasOnlyNumbersValidator()]],
    

    email: [{ value: this.user?.email, disabled: true }, [Validators.required, Validators.email]],
  })

  submitStatus = 0;
  registerStatus: number | undefined;
  location: any;

  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit(): void {
  }

  login() {
    this.hasError = false;
    this.auth.login(this.email, this.password).subscribe({
      next: data => this.location.back(),
      error: () => this.hasError = true
    });
  }

  get firstName() {
    return this.form.get('firstName');
  }
  get lastName() {
    return this.form.get('lastName');
  }
  get phone() {
    return this.form.get('phone');
  }
  get address() {
    return this.form.get('address');
  }
  get zipCode() {
    return this.form.get('zipCode');
  }
  get city() {
    return this.form.get('city');
  }
  get country() {
    return this.form.get('country');
  }

  register() {
    this.registerStatus = 0;
    if (this.user) {
      Object.assign(this.user, this.form.value);
      this.auth.update(this.user).subscribe({
        next: () =>this.registerStatus = 1,
        error: () =>this.registerStatus = -1
      });
    }
  }

}

