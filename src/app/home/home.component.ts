import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Page, Product } from '../entities';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnChanges {

  products?: Page<Product>;
  colors: string[] = [];
  colorSelected = "";
  genders: string[] = [];
  genderSelected = "";
  page = 0;
  pageSize = 10;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.fetchProducts();
    this.productService.showColors().subscribe(data => this.colors = data);
    this.productService.showGenders().subscribe(data => this.genders = data);
  }
  ngOnChanges() {
  }

  nextPage() {
    this.page++;
    this.fetchProducts();
  }
  previousPage() {
    this.page--;
    this.fetchProducts();
  }

  fetchProducts() {
    this.productService.showAllWithFilter(this.colorSelected, this.genderSelected, this.page, this.pageSize).subscribe(data => this.products = data);
  }

  selectColor(color: string) {
    this.colorSelected = color;
    this.fetchProducts();
  }

  selectGender(gender: string) {
    this.genderSelected = gender;
    this.fetchProducts();
  }
}
