import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function hasUpperValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value;
        const pattern = /[A-Z]+/.test(value);

        return value && !pattern ? { hasUpper: true } : null;
    }
}

export function hasLowerValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      const pattern = /[a-z]+/.test(value);

      return value && !pattern ? { hasLower: true } : null;
  }
}

export function hasDigitValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      const pattern = /[0-9]+/.test(value);

      return value && !pattern ? { hasDigit: true } : null;
  }
}

export function hasOnlyLettersAndSpaceValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value;
        const pattern = /^[A-Za-z',\-àâçèéêîôùû ]*$/.test(value);

        return value && !pattern ? { hasOnlyLettersAndSpace: true } : null;
    }
}

export function hasOnlyNumbersValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value;
        const pattern = /^(?:[0-9]+)?$/.test(value);

        return value && !pattern ? { hasOnlyNumbers: true } : null;
    }
}

export function pwdMatchValidator(control: AbstractControl): ValidationErrors | null {
  const password: string = control.get('password')?.value;
  const confirmPassword: string = control.get('repeatPassword')?.value;
  return password != confirmPassword ? { 'PasswordMatch': true } : null;
}
