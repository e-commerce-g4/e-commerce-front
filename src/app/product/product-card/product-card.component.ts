import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Design, Item, Product, Size } from 'src/app/entities';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-card[product]',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit, OnChanges {
  @Input()
  product?: Product;

  defaultImg: string = environment.defaultImgUrl;
  imgUrl: string = this.defaultImg;

  item = {} as Item;

  addSuccessfull = false;

  authState=this.auth.state;

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if (this.product) {
      if (this.product.image) {
        this.imgUrl = this.product.image;
      }
      if (!this.item.product) {
        this.item.product = this.product;
      }
    }
  }

  editSize(size: Size) {
    this.item = Object.assign({}, this.item);
    this.item.size = size;
  }

  editDesign(design: Design) {
    this.item = Object.assign({}, this.item);
    this.item.design = design;
  }

  editQuantity(quantity: number) {
    this.item = Object.assign({}, this.item);
    this.item.quantity = quantity;
  }

  addToCart(event: Event) {
    this.addSuccessfull = true;
  }
}
