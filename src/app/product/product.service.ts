import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Page, Product } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api/product';
  }

  public showAll(page = 0, pageSize = 10): Observable<Page<Product>> {
    return this.http.get<Page<Product>>(this.url, { params: { page, pageSize } });
  }

  public showAllWithFilter(color: string, gender: string, page = 0, pageSize = 10): Observable<Page<Product>> {
    let paramsMap = new Map<any, any>();
    if (color.length > 0) {
      paramsMap.set('color', color);
    }
    if (gender.length > 0) {
      paramsMap.set('gender', gender);
    }
    paramsMap.set('page', page);
    paramsMap.set('pageSize', pageSize);

    let params = new HttpParams();
    paramsMap.forEach((value: any, key: any) => {
      params = params.set(key, value);
    });

    return this.http.get<Page<Product>>(this.url, { params });
  }

  public showOne(id: number): Observable<Product> {
    return this.http.get<Product>(this.url + '/' + id)
  }

  public showColors(): Observable<string[]> {
    return this.http.get<string[]>(this.url + '/color')
  }

  public showGenders(): Observable<string[]> {
    return this.http.get<string[]>(this.url + '/gender')
  }

  update(product: Product, upload: File | undefined) {
    const form = new FormData();
    form.append('label', product.label);
    form.append('reference', product.reference);
    form.append('color', product.color);
    form.append('price', product.price.toString());
    form.append('gender', product.gender);
    if (upload) {
      form.append('upload', upload);
    }
    return this.http.put<Product>('/api/product/' + product.id, form);
  }

  add(product: Product, upload: File) {
    const form = new FormData();
    form.append('label', product.label);
    form.append('reference', product.reference);
    form.append('color', product.color);
    form.append('price', product.price.toString());
    form.append('gender', product.gender);
    form.append('upload', upload);
    return this.http.post<Product>('/api/product', form);
  }
}

