import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-product-filter[name][list]',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.css']
})
export class ProductFilterComponent implements OnInit {

  @Input()
  name="";
  @Input()
  list: string[] = [];

  @Output()
  selection: EventEmitter<string> = new EventEmitter();

  selected = "";

  constructor() { }

  ngOnInit(): void {
  }

  sendFilter() {
    this.selection.emit(this.selected);
  }

}
