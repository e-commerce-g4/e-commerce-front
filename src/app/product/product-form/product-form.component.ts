import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Product } from 'src/app/entities';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit, OnChanges {

  @Input()
  product?: Product;

  form?: FormGroup;

  upload?: File;

  constructor(private fb: FormBuilder,
    private prodService: ProductService,
    private router: Router) { }

  ngOnInit(): void {
    this.initForm()
  }

  ngOnChanges() {
    if (this.product && this.form?.untouched) {
      this.initForm()
    }
  }

  get label() {
    return this.form?.get('label');
  }
  get reference() {
    return this.form?.get('reference');
  }
  get price() {
    return this.form?.get('price');
  }
  get color() {
    return this.form?.get('color');
  }
  get gender() {
    return this.form?.get('gender');
  }

  initForm() {
    this.form = this.fb.group({
      label: [this.product?.label, [Validators.required, Validators.minLength(3)]],
      reference: [this.product?.reference, [Validators.required, Validators.minLength(3)]],
      price: [this.product?.price, [Validators.required, Validators.min(1)]],
      color: [this.product?.color, [Validators.required, Validators.minLength(3)]],
      gender: [this.product?.gender, [Validators.required, Validators.minLength(3)]],
    })
  }

  changeFile(event: any) {
    if (event.target.files.length > 0) {
      this.upload = event.target.files[0]
    }
  }

  submit() {
    if (this.product) {
      Object.assign(this.product, this.form?.value)
      this.prodService.update(this.product, this.upload).subscribe({
        next: data => this.router.navigate(['product', data.id])
      });
    } else {
      if (this.upload) {
        this.prodService.add(this.form?.value, this.upload).subscribe({
          next: data => this.router.navigate(['product', data.id])
        });
      }
    }
  }
}

