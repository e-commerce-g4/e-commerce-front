import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Size } from 'src/app/entities';

@Injectable({
  providedIn: 'root'
})
export class SizeService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api/size';
  }

  public showAll(): Observable<Size[]> {
    return this.http.get<Size[]>(this.url);
  }
}
