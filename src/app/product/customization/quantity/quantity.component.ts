import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-quantity',
  templateUrl: './quantity.component.html',
  styleUrls: ['./quantity.component.css']
})
export class QuantityComponent implements OnInit {

  maxQuantity: number = environment.maxQuantity;

  @Input()
  quantitySelected: number | undefined = 0;

  @Output()
  quantityEmit: EventEmitter<number> = new EventEmitter();

  constructor() { }
  ngOnInit(): void {
  }

  sendQuantity() {
    this.quantityEmit.emit(this.quantitySelected);
  }

}
