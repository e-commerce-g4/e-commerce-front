import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Design } from '../../../entities';
import { DesignService } from '../design.service';

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.css']
})
export class DesignComponent implements OnInit {

  designs: Design[] = [];
  designSelected?: Design;

  @Output()
  designEmit: EventEmitter<Design> = new EventEmitter();

  constructor(private designService: DesignService) { }

  ngOnInit(): void {
    this.designService.showAll().subscribe(data => this.designs = data)
  }

  sendDesign() {
    this.designEmit.emit(this.designSelected);
  }

}
