import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Size } from '../../../entities';
import { SizeService } from '../size.service';

@Component({
  selector: 'app-size',
  templateUrl: './size.component.html',
  styleUrls: ['./size.component.css']
})
export class SizeComponent implements OnInit {

  sizes: Size[] = [];
  sizeSelected?: Size;

  @Output()
  sizeEmit: EventEmitter<Size> = new EventEmitter();

  constructor(private sizeService: SizeService) { }

  ngOnInit(): void {
    this.sizeService.showAll().subscribe(data => this.sizes = data)
  }

  sendSize() {
    this.sizeEmit.emit(this.sizeSelected);
  }

}
