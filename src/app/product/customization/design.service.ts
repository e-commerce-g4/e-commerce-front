import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Design } from 'src/app/entities';

@Injectable({
  providedIn: 'root'
})
export class DesignService {

    private url: string;
  
    constructor(private http: HttpClient) {
      this.url = '/api/design';
    }
  
    public showAll(): Observable<Design[]> {
      return this.http.get<Design[]>(this.url);
    }
  }
