import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Design, Item, Product, Size } from 'src/app/entities';
import { environment } from 'src/environments/environment';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product?: Product;
  defaultImg: string = environment.defaultImgUrl;
  imgUrl: string = this.defaultImg;
  item = {} as Item;

  addSuccess = false;

  authState = this.auth.state;

  constructor(private route: ActivatedRoute,
    private productService: ProductService,
    private auth: AuthService) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.productService.showOne(params['id'])))
      .subscribe(data => {
        this.product = data;
        this.imgUrl = this.product.image;
        this.item.product = data;
      })
  }

  get total() {
    if (this.product?.price && this.item.quantity) {
      return this.product.price * this.item.quantity
    } else {
      return 0
    }

  }

  editSize(size: Size) {
    this.item = Object.assign({}, this.item);
    this.item.size = size;
  }

  editDesign(design: Design) {
    this.item = Object.assign({}, this.item);
    this.item.design = design;
  }

  editQuantity(quantity: number) {
    this.item = Object.assign({}, this.item);
    this.item.quantity = quantity;
  }

  addToCart(event: Event) {
    this.addSuccess = true;
  }

}
