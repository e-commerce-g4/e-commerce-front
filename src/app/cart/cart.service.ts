import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Design, Item, Order, Product, Size } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart: Item[] = [];

  maxQuantity: number = environment.maxQuantity;

  constructor() {
    const stored = localStorage.getItem('cart');
    if (stored) {
      this.cart = JSON.parse(stored);
    }
  }

  add(item: Item) {
    const index = this.findIndex(item);
    if (index < 0) {
      this.cart.push(item);
    } else {
      item.quantity += this.cart[index].quantity!;
      if (item.quantity > this.maxQuantity) {
        item.quantity = this.maxQuantity
      }
      this.cart[index].quantity = item.quantity;
    }
    this.updateCart();
  }

  update(item: Item) {
    const index = this.findIndex(item);
    if (index >= 0) {
      if (item.quantity > 0) {
        this.cart[index].quantity = item.quantity;
      } else {
        this.cart.splice(index, 1);
      }
    }
    this.updateCart();
  }

  deleteCart() {
    this.cart.splice(0, this.cart.length);
    this.updateCart();
  }

  private findIndex(item: Item): number {
    return this.cart.findIndex(data => data.product.id == item.product.id && data.size.id == item.size.id && data.design.id == item.design.id);
  }

  private updateCart() {
    if (this.cart.length > 0) {
      localStorage.setItem('cart', JSON.stringify(this.cart));
    } else {
      localStorage.removeItem('cart');
    }
  }
}
