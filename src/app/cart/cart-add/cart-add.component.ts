import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Item } from 'src/app/entities';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart-add[item]',
  templateUrl: './cart-add.component.html',
  styleUrls: ['./cart-add.component.css']
})
export class CartAddComponent implements OnInit, OnChanges {

  @Input()
  item?: Item;

  @Output()
  toCartEmit = new EventEmitter();

  enabled = false;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.enableButton()
  }

  addToCart() {
    if (this.checkSelection()) {
      this.cartService.add(this.item!);
      this.toCartEmit.emit();
    }
  }

  enableButton() {
    if (this.checkSelection()) {
      this.enabled = true;
    } else {
      this.enabled = false;
    }
  }

  checkSelection() {
    if (this.item && this.item.size && this.item.design && this.item.quantity > 0) {
      return true;
    } else {
      return false;
    }
  }

}
