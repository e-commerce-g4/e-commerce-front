import { Component, OnInit } from '@angular/core';
import { Item, Order } from 'src/app/entities';
import { CartService } from 'src/app/cart/cart.service';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.css']
})
export class CartDetailComponent implements OnInit {

  cart: Item[] = [];
  order?: Order

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.cart = this.cartService.cart;
  }

  deleteCart() {
    this.cartService.deleteCart();
  }

  get total() {
    let amount = 0;
    this.cart.forEach(item => amount += item.product.price * item.quantity)
    // Autre methode pour le total panier:
    // for (let index = 0; index < this.cart.length; index++) {
    //   amount +=  this.cart[index].price * this.cart[index].quantity ;
    // }
    return amount
  }
}
