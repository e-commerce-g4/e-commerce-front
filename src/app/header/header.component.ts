import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { CartService } from '../cart/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  cart = this.cartService.cart
  authState = this.auth.state;

  constructor(private auth: AuthService, private cartService: CartService) { }

  ngOnInit(): void {
  }

  logout() {
    this.auth.logout().subscribe();
  }


}
