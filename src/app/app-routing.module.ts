import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountAddressComponent } from './account/account-address/account-address.component';
import { AccountDetailComponent } from './account/account-detail/account-detail.component';
import { AccountEditComponent } from './account/account-edit/account-edit.component';
import { AccountComponent } from './account/account/account.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { CartDetailComponent } from './cart/cart-detail/cart-detail.component';
import { AuthenticatedGuard } from './auth/authenticated.guard';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { OrderCheckoutComponent } from './order/order-checkout/order-checkout.component';
import { OrderDetailComponent } from './order/order-detail/order-detail.component';
import { OrderPageComponent } from './order/order-page/order-page.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { RoleAdminGuard } from './auth/role-admin.guard';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'product', children: [
      { path: 'create', component: ProductCreateComponent, canActivate: [RoleAdminGuard] },
      { path: ':id/edit', component: ProductEditComponent, canActivate: [RoleAdminGuard] },
      { path: ':id', component: ProductDetailComponent },
    ]
  },
  { path: 'cart', component: CartDetailComponent },
  {
    path: 'order', canActivate: [AuthenticatedGuard], children: [
      { path: '', component: OrderCheckoutComponent }
    ]
  },
  {
    path: 'account', component: AccountComponent, canActivate: [AuthenticatedGuard], children: [
      {
        path: '', children: [
          { path: '', component: AccountDetailComponent },
          { path: 'edit', component: AccountEditComponent }
        ]
      },
      {
        path: 'order', children: [
          { path: '', component: OrderPageComponent },
          { path: ':id', component: OrderDetailComponent }
        ]
      },
      { path: 'address', component: AccountAddressComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
