import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order, Page } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api/order';
  }

  public showAll(page = 0, pageSize = 10): Observable<Page<Order>> {
    return this.http.get<Page<Order>>(this.url, { params: { page, pageSize } });
  }

  public showById(id: number): Observable<Order> {
    return this.http.get<Order>(this.url + '/' + id);
  }

  public add(order: Order): Observable<Order> {
    return this.http.post<Order>(this.url + '/new', order)
  }
}
