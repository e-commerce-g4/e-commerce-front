import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'src/app/entities';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.css']
})
export class OrderConfirmationComponent implements OnInit {

  @Input()
  order?: Order

  constructor() { }

  ngOnInit(): void {
  }

  get total() {
    let amount = 0;
    this.order?.items?.forEach(item => amount += item.product.price * item.quantity)
    return amount
  }
}
