import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AddressService } from 'src/app/address/address.service';
import { AuthService } from 'src/app/auth/auth.service';
import { CartService } from 'src/app/cart/cart.service';
import { Address, Order } from 'src/app/entities';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-order-validation',
  templateUrl: './order-validation.component.html',
  styleUrls: ['./order-validation.component.css']
})
export class OrderValidationComponent implements OnInit {

  newOrder = {} as Order;

  @Output()
  orderData: EventEmitter<Order> = new EventEmitter()

  constructor(private cartService: CartService,
    private auth: AuthService,
    private addressService: AddressService,
    private orderService: OrderService,
    private router: Router) { }


  ngOnInit(): void {
    this.newOrder.payment = "";
    if (this.auth.state.user) {
      this.newOrder.user = this.auth.state.user;
    }
    this.addressService.showAll().subscribe(data =>
      this.newOrder.user.addresses = data
    );
    this.newOrder.items = this.cartService.cart;
  }

  get total() {
    let amount = 0;
    this.newOrder.items?.forEach(item => amount += item.product.price * item.quantity)
    return amount
  }

  selectAddress(address: Address) {
    this.newOrder.address = address;
  }

  order() {
    this.orderService.add(this.newOrder).subscribe({
      next: data => {
        this.cartService.deleteCart();
        this.orderData.emit(data);
      },
      error: (e) => console.log(e)
    })
  }
}
