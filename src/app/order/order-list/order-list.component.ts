import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'src/app/entities';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {
  @Input()
  orders: Order[] | undefined = [];

  constructor() { }

  ngOnInit(): void {
  }

}
