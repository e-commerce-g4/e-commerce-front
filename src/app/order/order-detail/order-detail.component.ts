import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Order } from 'src/app/entities';
import { OrderService } from 'src/app/order/order.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {

  order?: Order;

  constructor(private route: ActivatedRoute, private orderService: OrderService) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.orderService.showById(params['id']))
    ).subscribe(data => this.order = data)
  }

  get total() {
    let amount = 0;
    if (this.order) {
      this.order.items?.forEach(item => {
        if (item.price) {
          amount += item.quantity * item.price
        } else {
          amount += item.quantity * item.product.price
        }
      })
    }
    return amount;
  }
}
