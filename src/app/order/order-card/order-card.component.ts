import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'src/app/entities';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.css']
})
export class OrderCardComponent implements OnInit {
  @Input()
  order?: Order;

  constructor() { }

  ngOnInit(): void {
  }

  get total() {
    let amount = 0;
    if (this.order) {
      this.order.items?.forEach(item => {
        if (item.price) {
          amount += item.quantity * item.price;
        } else {
          amount += item.quantity * item.product.price;
        }
      });
    }
    return amount;
  }

}
