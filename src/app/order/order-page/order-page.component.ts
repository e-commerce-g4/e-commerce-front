import { Component, OnInit } from '@angular/core';
import { Order, Page } from 'src/app/entities';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css']
})
export class OrderPageComponent implements OnInit {

  orders?: Page<Order>;
  page = 0;
  pageSize = 10;

  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.fetchOrders();
  }

  nextPage() {
    this.page++;
    this.fetchOrders();
  }
  previousPage() {
    this.page--;
    this.fetchOrders();
  }

  fetchOrders() {
    this.orderService.showAll(this.page, this.pageSize).subscribe(data => this.orders = data);
  }
}
