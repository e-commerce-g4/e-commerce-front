import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/cart/cart.service';
import { Order } from 'src/app/entities';

@Component({
  selector: 'app-order-checkout',
  templateUrl: './order-checkout.component.html',
  styleUrls: ['./order-checkout.component.css']
})
export class OrderCheckoutComponent implements OnInit {

  order?: Order;

  constructor(private cartService: CartService, private router: Router) { }

  ngOnInit(): void {
    if (this.cartService.cart.length == 0) {
      this.router.navigate(['/home'])
    }
  }

  getOrder(orderData: Order) {
    this.order = orderData;
  }
}
