import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { hasOnlyLettersAndSpaceValidator, hasOnlyNumbersValidator } from 'src/app/validators';

@Component({
  selector: 'app-account-edit-info',
  templateUrl: './account-edit-info.component.html',
  styleUrls: ['./account-edit-info.component.css']
})
export class AccountEditInfoComponent implements OnInit {

  user = this.auth.state.user;

  form = this.fb.group({
    firstName: [this.user?.firstName, [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    lastName: [this.user?.lastName, [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
    phone: [this.user?.phone, [Validators.required, Validators.minLength(10), Validators.maxLength(10), hasOnlyNumbersValidator()]],
    email: [{ value: this.user?.email, disabled: true }, [Validators.required, Validators.email]],
  })

  submitStatus = 0;

  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit(): void {
  }

  get firstName() {
    return this.form.get('firstName');
  }
  get lastName() {
    return this.form.get('lastName');
  }
  get phone() {
    return this.form.get('phone');
  }

  submit() {
    this.submitStatus = 0;
    if (this.user) {
      Object.assign(this.user, this.form.value);
      this.auth.update(this.user).subscribe({
        next: () =>this.submitStatus = 1,
        error: () =>this.submitStatus = -1
      });
    }
  }

}
