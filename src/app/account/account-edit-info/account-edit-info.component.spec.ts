import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountEditInfoComponent } from './account-edit-info.component';

describe('AccountEditInfoComponent', () => {
  let component: AccountEditInfoComponent;
  let fixture: ComponentFixture<AccountEditInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountEditInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountEditInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
