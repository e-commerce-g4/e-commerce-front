import { Component, OnInit } from '@angular/core';
import { Address } from 'src/app/entities';
import { AddressService } from 'src/app/address/address.service';

@Component({
  selector: 'app-account-address',
  templateUrl: './account-address.component.html',
  styleUrls: ['./account-address.component.css']
})
export class AccountAddressComponent implements OnInit {

  addresses: Address[] = []

  constructor(private addressService: AddressService) { }

  ngOnInit(): void {
    this.addressService.showAll().subscribe(data => this.addresses = data)
  }

  addAddress(address: Address) {
    this.addresses.push(address);
  }

  deleteAddress(address: Address) {
    this.addresses.splice(
      this.addresses.findIndex(a => a.id == address.id), 1)
  }

}
