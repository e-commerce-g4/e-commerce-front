import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/entities';

@Component({
  selector: 'app-account-info[user]',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.css']
})
export class AccountInfoComponent implements OnInit {

  @Input()
  user?: User | null;

  constructor() { }

  ngOnInit(): void {
  }

}
