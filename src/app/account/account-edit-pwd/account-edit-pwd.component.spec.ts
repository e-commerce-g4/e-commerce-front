import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountEditPwdComponent } from './account-edit-pwd.component';

describe('AccountEditPwdComponent', () => {
  let component: AccountEditPwdComponent;
  let fixture: ComponentFixture<AccountEditPwdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountEditPwdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountEditPwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
