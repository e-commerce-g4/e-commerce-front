import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { hasDigitValidator, hasLowerValidator, hasUpperValidator, pwdMatchValidator } from 'src/app/validators';

@Component({
  selector: 'app-account-edit-pwd',
  templateUrl: './account-edit-pwd.component.html',
  styleUrls: ['./account-edit-pwd.component.css']
})
export class AccountEditPwdComponent implements OnInit {

  form = this.fb.group({
    oldPassword: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(64),
      hasUpperValidator(), hasLowerValidator(), hasDigitValidator()]],
    repeatPassword: ['']
  }, { validators: pwdMatchValidator })

  submitStatus = 0;

  constructor(private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit(): void {
  }

  get oldPassword() {
    return this.form.get('oldPassword');
  }
  get password() {
    return this.form.get('password');
  }
  get repeatPassword() {
    return this.form.get('repeatPassword');
  }

  submit() {
    this.submitStatus = 0;
    this.auth.changePassword(this.form.value.oldPassword, this.form.value.password).subscribe({
      next: () => this.submitStatus = 1,
      error: () => this.submitStatus = -1
    });
  }

}
