import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/entities';

@Component({
  selector: 'app-account-nav[user]',
  templateUrl: './account-nav.component.html',
  styleUrls: ['./account-nav.component.css']
})
export class AccountNavComponent implements OnInit {

  @Input()
  user?: User | null;

  constructor() { }

  ngOnInit(): void {
  }

}
