import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Address } from 'src/app/entities';
import { hasOnlyLettersAndSpaceValidator, hasOnlyNumbersValidator } from 'src/app/validators';
import { AddressService } from '../address.service';

@Component({
  selector: 'app-address-edit',
  templateUrl: './address-edit.component.html',
  styleUrls: ['./address-edit.component.css']
})
export class AddressEditComponent implements OnInit {

  @Input()
  address?: Address

  @Output()
  newAddress: EventEmitter<Address> = new EventEmitter()

  modalId = "";
  form?: FormGroup;

  constructor(private fb: FormBuilder, private addressService: AddressService) { }

  ngOnInit(): void {
    if (this.address?.id) {
      this.modalId = this.address?.id.toString();
    }
    this.initForm()
  }

  get label() {
    return this.form?.get('label');
  }
  get street() {
    return this.form?.get('street');
  }
  get zipCode() {
    return this.form?.get('zipCode');
  }
  get city() {
    return this.form?.get('city');
  }
  get country() {
    return this.form?.get('country');
  }

  initForm() {
    this.form = this.fb.group({
      label: [this.address?.label, [Validators.required, Validators.minLength(3)]],
      street: [this.address?.street, [Validators.required, Validators.minLength(3)]],
      zipCode: [this.address?.zipCode, [Validators.required, Validators.minLength(5), Validators.maxLength(5), hasOnlyNumbersValidator()]],
      city: [this.address?.city, [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]],
      country: [this.address?.country, [Validators.required, Validators.minLength(3), hasOnlyLettersAndSpaceValidator()]]
    })
  }

  submit() {
    if (this.address) {
      Object.assign(this.address, this.form?.value);
      this.addressService.update(this.address).subscribe(data => this.address = data)
    } else {
      this.addressService.add(this.form?.value).subscribe(data => this.newAddress.emit(data))
    }
  }
}
