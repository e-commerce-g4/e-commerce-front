import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address } from 'src/app/entities';
import { AddressService } from '../address.service';

@Component({
  selector: 'app-address-card[address]',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.css']
})
export class AddressCardComponent implements OnInit {

  @Input()
  address?: Address

  @Input()
  edit = false

  @Output()
  deleteAddress: EventEmitter<Address> = new EventEmitter();

  constructor(private addressService: AddressService) { }

  ngOnInit(): void {
  }

  delete() {
    if (this.address?.id) {
      this.addressService.delete(this.address.id).subscribe(
        () => this.deleteAddress.emit(this.address)
      );
    }
  }
}
