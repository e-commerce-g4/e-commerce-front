import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Address } from 'src/app/entities';


@Component({
  selector: 'app-address-select[addresses]',
  templateUrl: './address-select.component.html',
  styleUrls: ['./address-select.component.css']
})
export class AddressSelectComponent implements OnInit, OnChanges {

  @Input()
  addresses: Address[] | undefined = [];

  addressSelected?: Address;

  @Output()
  addressEmit: EventEmitter<Address> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if (this.addresses && this.addresses.length > 0 && !this.addressSelected) {
      this.addressSelected = this.addresses[0];
      this.sendAddress();
    }
  }

  sendAddress() {
    this.addressEmit.emit(this.addressSelected);
  }

}
