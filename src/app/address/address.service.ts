import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Address } from '../entities';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = '/api/address';
  }

  public showAll(): Observable<Address[]> {
    return this.http.get<Address[]>(this.url);
  }

  public update(address: Address): Observable<Address> {
    return this.http.post<Address>(this.url + '/' + address.id,  address )
  }

  public add(address: Address): Observable<Address> {
    return this.http.post<Address>(this.url,  address )
  }

  public delete(id: number) {
    return this.http.delete<void>(this.url + '/' + id);
  }
}
