import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { CartService } from 'src/app/cart/cart.service';
import { Item } from 'src/app/entities';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.css']
})
export class ItemCardComponent implements OnInit, OnChanges {
  @Input()
  item?: Item;

  @Input()
  edit = false;

  subTotal = 0;

  defaultImg: string = environment.defaultImgUrl;
  imgUrl: string = this.defaultImg;
  designUrl: string = "";

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if (this.item?.product.image) {
      this.imgUrl = this.item.product.image;
    }
    this.refreshSubTotal();
  }

  editQuantity(quantity: number) {
    if (this.item) {
      this.item.quantity = quantity;
      this.cartService.update(this.item);
      this.refreshSubTotal();
    }
  }

  refreshSubTotal() {
    if (this.item && this.item.quantity && this.item.product.price) {
      this.subTotal = this.item.quantity * this.item.product.price;
    } else {
      this.subTotal = 0;
    }
  }

}
