import { Component, Input, OnInit } from '@angular/core';
import { Item } from 'src/app/entities';

@Component({
  selector: 'app-item-list[items]',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  @Input()
  items: Item[] | undefined = []

  @Input()
  edit = false;

  constructor() { }

  ngOnInit(): void {
  }

}
