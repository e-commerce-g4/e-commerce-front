import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import '@angular/common/locales/global/fr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProductCardComponent } from './product/product-card/product-card.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AccountComponent } from './account/account/account.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AddressCardComponent } from './address/address-card/address-card.component';
import { OrderConfirmationComponent } from './order/order-confirmation/order-confirmation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SizeComponent } from './product/customization/size/size.component';
import { QuantityComponent } from './product/customization/quantity/quantity.component';
import { DesignComponent } from './product/customization/design/design.component';
import { CartDetailComponent } from './cart/cart-detail/cart-detail.component';
import { CartAddComponent } from './cart/cart-add/cart-add.component';
import { CredentialInterceptor } from './credential.interceptor';
import { OrderDetailComponent } from './order/order-detail/order-detail.component';
import { OrderListComponent } from './order/order-list/order-list.component';
import { OrderPageComponent } from './order/order-page/order-page.component';
import { OrderCardComponent } from './order/order-card/order-card.component';
import { ItemListComponent } from './item/item-list/item-list.component';
import { ItemCardComponent } from './item/item-card/item-card.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { AccountNavComponent } from './account/account-nav/account-nav.component';
import { AddressSelectComponent } from './address/address-select/address-select.component';
import { OrderValidationComponent } from './order/order-validation/order-validation.component';
import { AccountDetailComponent } from './account/account-detail/account-detail.component';
import { ProductFilterComponent } from './product/product-filter/product-filter.component';
import { ProductFormComponent } from './product/product-form/product-form.component';
import { AccountInfoComponent } from './account/account-info/account-info.component';
import { AccountEditComponent } from './account/account-edit/account-edit.component';
import { AccountEditInfoComponent } from './account/account-edit-info/account-edit-info.component';
import { AccountEditPwdComponent } from './account/account-edit-pwd/account-edit-pwd.component';
import { AccountAddressComponent } from './account/account-address/account-address.component';
import { AddressEditComponent } from './address/address-edit/address-edit.component';
import { OrderCheckoutComponent } from './order/order-checkout/order-checkout.component';
import { ServerFilePipe } from './server-file.pipe';
import { ProductCreateComponent } from './product/product-create/product-create.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    ProductCardComponent,
    ProductListComponent,
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    AddressCardComponent,
    OrderConfirmationComponent,
    NotFoundComponent,
    SizeComponent,
    QuantityComponent,
    DesignComponent,
    CartDetailComponent,
    CartAddComponent,
    ItemCardComponent,
    OrderDetailComponent,
    OrderListComponent,
    OrderPageComponent,
    OrderCardComponent,
    ItemListComponent,
    ProductDetailComponent,
    AccountNavComponent,
    AddressSelectComponent,
    OrderValidationComponent,
    AccountDetailComponent,
    ProductFilterComponent,
    ProductFormComponent,
    AccountInfoComponent,
    AccountEditComponent,
    AccountEditInfoComponent,
    AccountEditPwdComponent,
    AccountAddressComponent,
    AddressEditComponent,
    OrderCheckoutComponent,
    ServerFilePipe,
    ProductCreateComponent,
    ProductEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: CredentialInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
