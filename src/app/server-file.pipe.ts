import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'serverFile'
})
export class ServerFilePipe implements PipeTransform {

  transform(value: string): string {
    if(value.startsWith('/assets')) {
      return value;
    }
    return environment.serverUrl + '/uploads/' + value;

  }

}
